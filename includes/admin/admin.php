<?php
/**
 * @package  WooDigitalRiverPlugin
 */

namespace Includes\Admin;

use \Includes\Base\BaseController;

class Admin extends BaseController
{
	
	public function register() 
	{
		add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
	}

	public function add_admin_pages() 
	{
		add_menu_page( 'Woo DigitalRiver Plugin', 'Woo Digital River', 'manage_options', 'woo_digitalriver_plugin', array( $this, 'admin_index' ), 'dashicons-money', 110 );
	}

	public function admin_index() 
	{
		require_once $this->plugin_path . 'templates/admin.php';
	}
    
}